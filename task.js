function Square(height, width) {
	this.height = height;
	this.width = width;

	this.getArea = function() {
		this.area = this.height * this.width;
		return this.area;
	}
}
var figure1 = new Square(20,20);
//console.log(figure1.getArea());
var figure2 = new Square(10,10);
//console.log(figure2.getArea());


function Circle(radius) {
	Square.call(this, arguments);
	this.height = Math.pow(radius, 2);
	this.width = Math.PI;
}
var circle1 = new Circle(10);
//console.log(circle1.getArea());


function Area() {
	this._figures = [];
	this.addFigure = function(figure){
		this._figures.push(figure);
		return this._figures;
	}
	this.clear = function(){
		this._figures.length=0;
		return this._figures;
	}
	Object.defineProperty(this, 'size', {
		get: function(){
			this._figures.forEach(function(item, i){
				console.log (item.getArea());
			});
		}
	}); 
}

var area1 = new Area();
area1.addFigure(figure1);
area1.addFigure(figure2);
area1.addFigure(circle1);
//area1.clear();
console.log("Size:", area1.size);